#!/bin/python3
import pygame as pg
pg.init()
size = width, height = 1800,1400
screen = pg.display.set_mode(size)
zoom = 1
rsize = rw,rh = 1.8/zoom,1.4/zoom
dec = dex,dey = 0.5,0
accuracy = 500
two = 2
eh = 0
screen.fill((255,255,255))
pg.display.flip()
def abst(x):
    try :
        return abs(x)
    except OverflowError:
        return 5000
for b in range(height):
    for a in range(width):
        trc = complex(((a-width/2)/(width/(2*rw)))-dex,((b-height/2)/(height/(2*rh)))-dey)
        trc2 = trc
        for i in range(accuracy):
            trc2 = trc2**two + trc
            if abst(trc2) >= 2 :
                pg.draw.rect(screen,((255-4*i)%256,(255-16*i)%256,(255-32*i)%256),(a,b,eh,eh))
                break
        else:
            pg.draw.rect(screen,((160-(160/(2**(1/2))*abst(trc2)**(1/2))),(-8*abst(trc2)+16),(-32*abst(trc2)+64)),(a,b,eh,eh))
    pg.display.flip()
while True:
    for i in pg.event.get():
        if i.type == pg.QUIT:
            pg.quit()
            quit()
