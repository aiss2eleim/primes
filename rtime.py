import datetime
t = str(datetime.datetime.now().time()).split(":")
a = "PM"
if int(t[0]) < 12:
    a = "AM"
print(str(((int(t[0])-1)%12)+1)+"h "+str(round((int(t[1])*60+float(t[2]))/36,2))+(5-len(str(round((int(t[1])*60+float(t[2]))/36,2))))*"0"+"% "+a+"  |  ")