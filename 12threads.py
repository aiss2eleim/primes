#!/bin/python3
import multiprocessing as mp
def primeLI(bnd):
    pl = [2]
    for k in range(3,bnd+1):
        tmp = k**0.5
        for l in pl:
            if l <= tmp :
                if k%l == 0 :
                    break
            else:
                pl.append(k)
                break   
    return pl
def primeL(ListPs,primB,primLI):
    F = open("prlistmP"+str(primB)+"-"+str(primLI)+".txt","w")
    if primB<2:
        primB=2
    ret = []
    for i in range(primB,primLI+1):
        tmp = i**0.5
        for j in ListPs:
            if j <= tmp :
                if i%j == 0 :
                    break
            else:
                ret.append(i)
                break
    F.write(str(ret))
    F.close()
if __name__ == '__main__':
    x , y = int(input("de>")),int(input("a(~)>"))
    pli, t = primeLI(int(y**0.5)+1) , int(input("threads>"))
    for i in range(t):
        mp.Process(target=primeL, args=(pli,round(i*(y-x)/t)+x, round((i+1)*(y-x)/t)+x)).start() 
